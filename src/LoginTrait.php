<?php

namespace weitzman\LoginTrait;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\user\Entity\User;

/**
 * An alternative login/logout for sites that use TFA, SAML, etc.
 */
trait LoginTrait
{
    /**
     * Login via a one time password URL.
     *
     * @param \Drupal\Core\Session\AccountInterface $account
     *                                                       The account interface.
     */
    protected function drupalLogin(AccountInterface $account)
    {
        if ($this->loggedInUser) {
            $this->drupalLogout();
        }

        // These 3 lines are the only lines changed from parent::drupalLogin().
        $account = User::load($account->id()); // Reload to get latest login timestamp.
        $login = $this->user_pass_reset_url($account).'/login';
        $this->getSession()->visit($login);

        // @see ::drupalUserIsLoggedIn()
        $account->sessionId = $this->getSession()->getCookie(\Drupal::service('session_configuration')->getOptions(\Drupal::request())['name']);
        $this->assertTrue($this->drupalUserIsLoggedIn($account), new FormattableMarkup('User %name successfully logged in.', ['%name' => $account->getAccountName()]));

        $this->loggedInUser = $account;
        $this->container->get('current_user')->setAccount($account);
    }

    /**
     * Logout without asserting user/pass form fields.
     */
    protected function drupalLogout()
    {
        $this->drupalGet('user/logout');
        // Omit checking for user/pass fields.

        // Clear entity cache entry so drupalLogin() goes to DB.
        \Drupal::entityManager()
            ->getStorage('user')
            ->resetCache([$this->loggedInUser->id()]);

        // @see BrowserTestBase::drupalUserIsLoggedIn()
        unset($this->loggedInUser->sessionId);
        $this->loggedInUser = false;
        \Drupal::currentUser()->setAccount(new AnonymousUserSession());
    }

    /**
     * Generates a unique URL for a user to log in and reset their password.
     *
     * The only change here is use of time() instead of REQUEST_TIME.
     *
     * @param \Drupal\user\UserInterface $account
     *                                            An object containing the user account.
     * @param array                      $options
     *                                            (optional) A keyed array of settings. Supported options are:
     *                                            - langcode: A language code to be used when generating locale-sensitive
     *                                            URLs. If langcode is NULL the users preferred language is used.
     *
     * @return string
     *                A unique URL that provides a one-time log in for the user, from which
     *                they can change their password.
     */
    public function user_pass_reset_url($account, $options = [])
    {
        $timestamp = time();
        $langcode = isset($options['langcode']) ? $options['langcode'] : $account->getPreferredLangcode();

        return \Drupal::url(
            'user.reset',
            [
                'uid'       => $account->id(),
                'timestamp' => $timestamp,
                'hash'      => user_pass_rehash($account, $timestamp),
            ],
            [
                'absolute' => true,
                'language' => \Drupal::languageManager()->getLanguage($langcode),
            ]
        );
    }
}
