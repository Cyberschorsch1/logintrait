# LoginTrait for Drupal Test Traits

An add-on for [Drupal Test Traits](https://gitlab.com/weitzman/drupal-test-traits). 

Provides login/logout via user reset URL instead of forms. Useful when TFA/SAML are enabled.

## Installation

- Install via Composer. `composer require weitzman/logintrait`.

## Usage

- Use this trait whenever your test wants to login. See [ExampleLoginTest](https://gitlab.com/weitzman/logintrait/-/blob/master/src/ExampleLoginTest.php). 

## Colophon

- **Author**: Created by [Moshe Weitzman](http://weitzman.github.io).
- **License**: Licensed under the [MIT license][mit]

[mit]: ./LICENSE
